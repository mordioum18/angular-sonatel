FROM openshift/base-centos7

LABEL summary="Platform for building and running Angular applications" \
      io.k8s.description="OpenShift S2I builder image for Angular apps using Angular CLI and Apache httpd 2.4." \
      io.k8s.display-name="Angular S2I httpd" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,angular" \
      com.redhat.dev-mode="DEV_MODE:false" \
      com.redhat.deployments-dir="/opt/app-root/src" \
      com.redhat.dev-mode.port="DEBUG_PORT:5858"

EXPOSE 8080

ENV NODE_VERSION=10.16.0 \
  NPM_CONFIG_LOGLEVEL=info \
  NPM_CONFIG_PREFIX=$HOME/.npm-global \
  PATH=$HOME/node_modules/.bin/:$HOME/.npm-global/bin/:$PATH \
  NPM_VERSION=6.9.0 \
  DEBUG_PORT=5858 \
  NODE_ENV=production \
  DEV_MODE=false

RUN yum install -y \
https://www.softwarecollections.org/repos/rhscl/httpd24/epel-7-x86_64/noarch/rhscl-httpd24-epel-7-x86_64-1-2.noarch.rpm && \
  yum install -y --setopt=tsflags=nodocs httpd24 && \
  yum clean all -y

RUN   yum install -y epel-release && \
      INSTALL_PKGS="bzip2 nss_wrapper" && \
      yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
      rpm -V $INSTALL_PKGS && \
      yum clean all -y && \
      curl -o node-v${NODE_VERSION}-linux-x64.tar.gz -sSL https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
      curl -o SHASUMS256.txt.asc -sSL https://nodejs.org/dist/v${NODE_VERSION}/SHASUMS256.txt.asc && \
      gpg --batch -d SHASUMS256.txt.asc | grep " node-v${NODE_VERSION}-linux-x64.tar.gz\$" | sha256sum -c - && \
      tar -zxf node-v${NODE_VERSION}-linux-x64.tar.gz -C /usr/local --strip-components=1 && \
      npm install -g npm@${NPM_VERSION} && \
      find /usr/local/lib/node_modules/npm -name test -o -name .bin -type d | xargs rm -rf; \
      rm -rf ~/node-v${NODE_VERSION}-linux-x64.tar.gz ~/SHASUMS256.txt.asc /tmp/node-v${NODE_VERSION} ~/.npm ~/.node-gyp ~/.gnupg \
        /usr/share/man /tmp/* /usr/local/lib/node_modules/npm/man /usr/local/lib/node_modules/npm/doc /usr/local/lib/node_modules/npm/html

COPY ./s2i/bin/ $STI_SCRIPTS_PATH
COPY ./contrib/ /opt/app-root
RUN chmod -R 777 $STI_SCRIPTS_PATH

RUN sed -i -f /opt/app-root/etc/httpdconf.sed /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf && \
    head -n151 /opt/rh/httpd24/root/etc/httpd/conf/httpd.conf | tail -n1 | grep "AllowOverride All" || exit && \
    chmod -R a+rwx /opt/rh/httpd24/root/var/run/httpd && \
    chown -R 1001:1001 /opt/app-root
USER 1001

CMD $STI_SCRIPTS_PATH/usage